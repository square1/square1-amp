<?php
namespace Square1\Amp\Transformers\Traits;

/**
 * Amp scripts library
 */
trait AmpScripts
{
    protected function getScriptTag($script)
    {
        switch ($script) {
            case 'amp-carousel':
                return '<script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>';
                break;
            case 'amp-youtube':
                return '<script async custom-element="amp-youtube" src="https://cdn.ampproject.org/v0/amp-youtube-0.1.js"></script>';
                break;
            case 'amp-iframe':
                return '<script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>';
                break;
            case 'amp-twitter':
                return '<script async custom-element="amp-twitter" src="https://cdn.ampproject.org/v0/amp-twitter-0.1.js"></script>';
                break;
            case 'amp-instagram':
                return '<script async custom-element="amp-instagram" src="https://cdn.ampproject.org/v0/amp-instagram-0.1.js"></script>';
            case 'amp-vine':
                return '<script async custom-element="amp-vine" src="https://cdn.ampproject.org/v0/amp-vine-0.1.js"></script>';
            case 'amp-vimeo':
                return '<script async custom-element="amp-vimeo" src="https://cdn.ampproject.org/v0/amp-vimeo-0.1.js"></script>';
            case 'amp-facebook':
                return '<script async custom-element="amp-facebook" src="https://cdn.ampproject.org/v0/amp-facebook-0.1.js"></script>';
            case 'amp-form':
                return '<script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>';
            case 'amp-apester-media':
                return '<script async custom-element="amp-apester-media" src="https://cdn.ampproject.org/v0/amp-apester-media-0.1.js"></script>';
            case 'amp-playbuzz':
                return '<script async custom-element="amp-playbuzz" src="https://cdn.ampproject.org/v0/amp-playbuzz-0.1.js"></script>';
            case 'amp-brid-player':
                return '<script async custom-element="amp-brid-player" src="https://cdn.ampproject.org/v0/amp-brid-player-0.1.js"></script>';
            case 'amp-jwplayer':
                return '<script async custom-element="amp-jwplayer" src="https://cdn.ampproject.org/v0/amp-jwplayer-0.1.js"></script>';
            case 'amp-video':
                return '<script async custom-element="amp-video" src="https://cdn.ampproject.org/v0/amp-video-0.1.js"></script>';
            case 'amp-audio':
                return '<script async custom-element="amp-audio" src="https://cdn.ampproject.org/v0/amp-audio-0.1.js"></script>';
            case 'amp-riddle-quiz':
                return '<script async custom-element="amp-riddle-quiz" src="https://cdn.ampproject.org/v0/amp-riddle-quiz-0.1.js"></script>';
            case 'amp-image-slider':
                return '<script async custom-element="amp-image-slider" src="https://cdn.ampproject.org/v0/amp-image-slider-0.1.js"></script>';
            case 'amp-riddle-quiz':
                return '<script async custom-element="amp-riddle-quiz" src="https://cdn.ampproject.org/v0/amp-riddle-quiz-0.1.js"></script>';
            default:
                break;
        }
    }
}
