<?php

namespace Square1\Amp\Transformers;

use Square1\Amp\Transformers\Interfaces\Transformable as Transformable;

/**
* Abstracract transformer class
*/
abstract class AbstractTransformer implements Transformable
{
    protected $data;
    protected $scripts = [];

    /**
     * Set the data to work with
     *
     * @param mixed $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the google-amp post-content (body) version
     *
     * @return mixed
     */
    public function getFormattedContent($html = '')
    {
        if (!empty($html)) {
            return $this->process($this->getRawContent($html));
        }
        return $this->process($this->getRawContent());
    }

    /**
     * Return the given data as it is
     *
     * @return mixed $data
     */
    public function getRawData()
    {
        return $this->data;
    }
    /**
     * Process the given post content and turns
     * the post-body into a google-amp compatible version
     *
     * @param  mixed $content post content (body)
     *
     * @return mixed $content post content formatted
     */
    abstract protected function process($content);

    /**
     * Get google-amp "<script>" tags
     *
     * @return string $scripts Google amp-script tags
     */
    public function getScripts()
    {
        return implode("\n", array_unique($this->scripts));
    }
}
