<?php

namespace Tests;

use Square1\Amp\AmpPost;
use Square1\Amp\Transformers\PublisherPlusTransformer;

/**
* AMP formatter test
*/
class TweetEmbedTest extends TestCase
{

    public function testTweetsShouldUseAmpTag()
    {
        $post = $this->getPost($this->getShortTweet());

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'parseTwitterQuotes', [$post['content']['formatted']]);

        $this->assertEquals($this->getShortTweetFormatted(), $formatted);

        // twitter script should be loaded
        $this->assertTrue(is_int(strpos($transformer->getScripts(), 'amp-twitter')), true);

    }

    private function getShortTweet()
    {
        return '<blockquote class="twitter-tweet">'.
            '<p lang="en" dir="ltr">Spot on. <a href="https://t.co/SXAibya2FZ">https://t.co/SXAibya2FZ</a></p>'.
            '<p>— Jamie Carragher (@Carra23) <a href="https://twitter.com/Carra23/status/808300781682233344">December 12, 2016</a></p>'.
            '</blockquote>'.
            '<p><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></p>';
    }

    private function getShortTweetFormatted()
    {
        return '<amp-twitter width=486 height=657
            layout="responsive"
            data-tweetid="808300781682233344">
            <blockquote placeholder
             class="twitter-tweet">'.
            '<p lang="en" dir="ltr">Spot on. <a href="https://t.co/SXAibya2FZ">https://t.co/SXAibya2FZ</a></p>'.
            '<p>— Jamie Carragher (@Carra23) <a href="https://twitter.com/Carra23/status/808300781682233344">December 12, 2016</a></p>'.
            '</blockquote>
        </amp-twitter><p></p>';
    }

}
