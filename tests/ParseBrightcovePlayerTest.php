<?php

namespace Tests;

use Square1\Amp\Transformers\PublisherPlusTransformer;

/**
 * AMP formatter test
 */
class ParseBrightcovePlayerTest extends TestCase
{
    public function testParseBrightcovePlayer()
    {
        $post = $this->getPost($this->getBrightcovePlayerEmbed());

        $transformer = new PublisherPlusTransformer($post);

        $unsupportedContent = $this->invokeMethod($transformer, 'getUnsupportedContent', ['square1.io']);

        $formatted = $this->invokeMethod($transformer, 'parseBrightcovePlayer', [$post['content']['formatted'], 'square1.io']);

        $this->assertEquals($unsupportedContent, $formatted);
    }

    private function getBrightcovePlayerEmbed()
    {
        return '<embed id="flashObj" width="480" height="270" type="application/x-shockwave-flash" src="https://c.brightcove.com/services/viewer/federated_f9?isVid=1&amp;isUI=1" flashvars="videoId=2731117004001&amp;linkBaseURL=http%3A%2F%2Fau.news.yahoo.com%2Fvic%2Fa%2F19320056%2Fmark-chopper-read-dead-report%2F&amp;playerID=2513628634001&amp;playerKey=AQ~~,AAACKW9LH8k~,A7HfECo5t7DrAeaToOVr5sEdpPjJEcCi&amp;domain=embed&amp;dynamicStreaming=true" base="http://admin.brightcove.com" seamlesstabbing="false" allowfullscreen="allowfullscreen" swliveconnect="true" allowscriptaccess="always" flashvars="videoId=2731117004001&amp;linkBaseURL=http%3A%2F%2Fau.news.yahoo.com%2Fvic%2Fa%2F19320056%2Fmark-chopper-read-dead-report%2F&amp;playerID=2513628634001&amp;playerKey=AQ~~,AAACKW9LH8k~,A7HfECo5t7DrAeaToOVr5sEdpPjJEcCi&amp;domain=embed&amp;dynamicStreaming=true" allowfullscreen="allowfullscreen" allowscriptaccess="always" swliveconnect="true" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash" bgcolor="#FFFFFF" />';
    }
}