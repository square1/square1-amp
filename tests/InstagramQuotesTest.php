<?php

namespace Tests;

use Square1\Amp\AmpPost;
use Square1\Amp\Transformers\PublisherPlusTransformer;

/**
* AMP formatter test
*/
class InstagramQuotesTest extends TestCase
{

    public function testTweetsShouldUseAmpTag()
    {
        $post = $this->getPost($this->getInstagramQuote());

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'parseInstagramQuotes', [$post['content']['formatted']]);

        $this->assertEquals($this->getInstagramQuoteFormatted(), $formatted);

        // twitter script should be loaded
        $this->assertTrue(is_int(strpos($transformer->getScripts(), 'amp-instagram')), true);
    }

    private function getInstagramQuote()
    {
        return '<blockquote class="instagram-media" style="background: #FFF; border: 0; border-radius: 3px; box-shadow: 0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width: 658px; padding: 0; width: calc(100% - 2px);" data-instgrm-captioned="" data-instgrm-version="7">
<div style="padding: 8px;">
<div style="background: #F8F8F8; line-height: 0; margin-top: 40px; padding: 50.0% 0; text-align: center; width: 100%;"></div>
<p style="margin: 8px 0 0 0; padding: 0 4px;"><a style="color: #000; font-family: Arial,sans-serif; font-size: 14px; font-style: normal; font-weight: normal; line-height: 17px; text-decoration: none; word-wrap: break-word;" href="https://www.instagram.com/p/BN2KpI5j-eC/" target="_blank">A very hard fought battle from the team today. Thanks to the travelling fans for getting behind us. We\'ll continue to fight &amp; not give up ???? #CPFC #EPL #GodIsGood</a></p>
<p style="color: #c9c8cd; font-family: Arial,sans-serif; font-size: 14px; line-height: 17px; margin-bottom: 0; margin-top: 8px; overflow: hidden; padding: 8px 0 7px; text-align: center; text-overflow: ellipsis; white-space: nowrap;">A photo posted by Wilfried Dazet Armel Zaha (@longlivezaha) on <time style="font-family: Arial,sans-serif; font-size: 14px; line-height: 17px;" datetime="2016-12-10T18:16:21+00:00">Dec 10, 2016 at 10:16am PST</time></p>
</div>
</blockquote>
<p><script async defer src="//platform.instagram.com/en_US/embeds.js"></script></p>';
    }

    private function getInstagramQuoteFormatted()
    {
        return '<amp-instagram
                data-shortcode="BN2KpI5j-eC"
                width="320"
                height="322"
                data-captioned
                layout="responsive">
            </amp-instagram>
';
    }
}
