<?php

namespace Tests;

use Square1\Amp\AmpPost;
use Square1\Amp\Transformers\PublisherPlusTransformer;

/**
* AMP formatter test
*/
class ParseHtmlFormsTest extends TestCase
{

    public function testFormTagsShoulduseAmpFormat()
    {
        $post = $this->getPost($this->getForm());

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'parseHtmlForms', [$post['content']['formatted'], 'square1.io']);

        // twitter script should be loaded
        $this->assertTrue(is_int(strpos($transformer->getScripts(), 'amp-form')), true);

        $this->assertEquals($this->getFormFormatted(), $formatted);
    }

    public function testMailchimpFormsAreNotSupported()
    {
        $post = $this->getPost($this->getMailChimpForm());

        $transformer = new PublisherPlusTransformer($post);

        $unsupportedContent = $this->invokeMethod($transformer, 'getUnsupportedContent', ['square1.io']);

        $formatted = $this->invokeMethod($transformer, 'parseHtmlForms', [$post['content']['formatted'], 'square1.io']);

        $this->assertEquals($unsupportedContent, $formatted);
    }

    private function getForm()
    {
        return '<form action="square1.io/post" method="post">
            <input type="text" name="name" placeholder="name">
            <button type="submit">send</button>
            </form>';
    }

    private function getFormFormatted()
    {
        return '<form action-xhr="https://square1.io/post" method="post" target="_blank">
            <input type="text" name="name" placeholder="name">
            <button type="submit">send</button>
            </form>';
    }

    private function getMailChimpForm()
    {
        return '<form class="validate" id="mc-embedded-subscribe-form" action="http://balls.us5.list-manage.com/subscribe/post?u=b06677a7a80a019a7ef7fe67d&amp;id=a4d9ee9e54" method="post" name="mc-embedded-subscribe-form" novalidate="" target="_blank">
            <h2>Join The New Balls Daily Mega Multi Morning Mailing List</h2>
            <div class="indicates-required"><span class="asterisk">*</span> indicates required</div>
            <div class="mc-field-group"><label for="mce-EMAIL">Email Address <span class="asterisk">*</span><br />
            </label><br />
            <input class="required email" id="mce-EMAIL" type="email" name="EMAIL" value="" /></div>
            <div class="clear" id="mce-responses"></div>
            <div class="clear"><input class="button" id="mc-embedded-subscribe" type="submit" name="subscribe" value="Subscribe" /></div>
            </form>';
    }
}
