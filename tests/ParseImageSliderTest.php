<?php

namespace Tests;

use Square1\Amp\AmpPost;
use Square1\Amp\Transformers\PublisherPlusTransformer;

/**
 * AMP formatter test
 */
class ParseImageSliderTest extends TestCase
{

    public function testParseImageSlider()
    {
        $post = $this->getPost($this->getImagesForSlider());

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'parseImageSlider', [$post['content']['formatted']]);

        $this->assertEquals($this->getImageSliderFormatted(), $formatted);

        // image slider script should be loaded
        $this->assertTrue(is_int(strpos($transformer->getScripts(), 'amp-image-slider')), true);
    }

    private function getImagesForSlider()
    {
        return '<div class="data-embed-slider slider-container twentytwenty-container"><p><img src="https://m0.joe.co.uk/wp-content/uploads/2018/04/19131529/hmirren.jpg" width="undefined" height="undefined" /><img src="https://m0.joe.co.uk/wp-content/uploads/2018/04/19131529/jlawrence.jpg" width="undefined" height="undefined" /></p>';
    }

    private function getImageSliderFormatted()
    {
        return '<div class="data-embed-slider slider-container twentytwenty-container"><p><amp-image-slider width="300" height="270" layout="responsive"><amp-img src="https://m0.joe.co.uk/wp-content/uploads/2018/04/19131529/hmirren.jpg" layout="fill" class="image"></amp-img><amp-img src="https://m0.joe.co.uk/wp-content/uploads/2018/04/19131529/jlawrence.jpg" layout="fill" class="image"></amp-img></amp-image-slider></p>';
    }
}