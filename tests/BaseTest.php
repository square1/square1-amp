<?php

namespace Tests;

use Square1\Amp\AmpPost;
use Square1\Amp\Transformers\PublisherPlusTransformer;

/**
* AMP formatter test
*/

class BaseTest extends TestCase
{

    /**
     * Test formatting content anchord tags inside strong tags
     * anchord tags inside strong tags have to be ignored
     *
     * @return void
     */
    public function testAttributesShouldBeAccesible()
    {
        $post = $this->getPost();

        $amp = $this->getAmpInstance($post);

        $this->assertEquals($post['content']['formatted'], $amp->content);

        $this->assertEquals($post, $amp->original);
    }

    /**
     * Allows to format any other content of the post object
     * like $post['footer']['html'] having access to the post data
     *
     * @return void
     */
    public function testPostContentCanBeSet()
    {
        $post = $this->getPost();
        $amp = $this->getAmpInstance($post);
        $customContent = '<h1>New content</h1>';

        $this->assertEquals($amp->getFormattedContent($customContent), $customContent);
    }
}
