<?php

namespace Tests;

use Square1\Amp\Transformers\PublisherPlusTransformer;

class RemoveEmbedTagsFromTubechopTest extends TestCase
{
    public function testRemoveEmbedTagsFromTubechop()
    {
        $post = $this->getPost($this->getContentWithTagsFromTubechop());

        $transformer = new PublisherPlusTransformer($post);

        $unsupportedContent = $this->invokeMethod($transformer, 'getUnsupportedContent', ['square1.io']);

        $formatted = $this->invokeMethod($transformer, 'removeEmbedTagsFromTubechop', [$post['content']['formatted'], 'square1.io']);

        $this->assertEquals($unsupportedContent, $formatted);
    }

    private function getContentWithTagsFromTubechop()
    {
        return '<embed width="425" height="344" type="application/x-shockwave-flash" src="https://swf.tubechop.com/tubechop.swf?vurl=unEmG6AnlaY&amp;start=43&amp;end=80&amp;cid=1577334" allowfullscreen="allowfullscreen" />';
    }
}