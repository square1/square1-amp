<?php

namespace Tests;

use Square1\Amp\AmpPost;
use Square1\Amp\Transformers\PublisherPlusTransformer;

/**
* AMP formatter test
*/
class ParseVideoEmbedsTest extends TestCase
{
    public function testParseVideoEmbeds()
    {
        $post = $this->getPost($this->getVideoEmbed());

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'parseVideoEmbeds', [$post['content']['formatted']]);

        $this->assertEquals($this->getFormattedVideo(), $formatted);

        $this->assertTrue(is_int(strpos($transformer->getScripts(), 'amp-video')), true);
    }

    private function getVideoEmbed()
    {
        return '<video src="https://media.buzz.ie/uploads/2016/03/12064937_1151473744972993_1275792886_n.mp4" controls="controls"></video>';
    }

    private function getFormattedVideo()
    {
        return '<amp-video controls
                            width="640"
                            height="360"
                            layout="responsive">
                            <source src="https://media.buzz.ie/uploads/2016/03/12064937_1151473744972993_1275792886_n.mp4"/>
                            <div fallback>
                                <p>This browser does not support the video element.</p>
                            </div>
                        </amp-video>';
    }
}
