<?php

namespace Tests;

use Square1\Amp\AmpPost;
use Square1\Amp\Transformers\PublisherPlusTransformer;

/**
* AMP formatter test
*/
class PlayBuzzEmbedTest extends TestCase
{
    public function testPlayBuzzEmbedsShouldUsePlayBuzzTags()
    {
        foreach ($this->getBeforeandAfterHTML() as $content => $expected) {
            $post = $this->getPost($content);

            $transformer = new PublisherPlusTransformer($post);

            $formatted = $this->invokeMethod($transformer, 'parsePlayBuzzEmbeds', [$post['content']['formatted'], 'square1.io']);

            $this->assertEquals($expected, $formatted);

            // iframe script should be loaded
            $this->assertTrue(is_int(strpos($transformer->getScripts(), 'amp-playbuzz')), true);
        }
    }

    private function getBeforeandAfterHTML()
    {
        return [
            // data game before data-embed-by
            '<div class="pb_feed" data-height="auto" data-game="/pjbrowne10/can-you-identify-the-lesser-known-irish-premier-league-player" data-tags="All" data-recommend="false" data-margin-top="0" data-game-info="true" data-comments="false" data-shares="true" data-embed-by="7d84fda5-2c5f-4c8a-b335-0e11a71497b7" data-key="Default"></div>' =>
            '<amp-playbuzz width=300 height=300 layout="responsive" src="https://playbuzz.com/pjbrowne10/can-you-identify-the-lesser-known-irish-premier-league-player?feed=true&src=http://playbuzz.com/pjbrowne10/can-you-identify-the-lesser-known-irish-premier-league-player&game=pjbrowne10/can-you-identify-the-lesser-known-irish-premier-league-player&social=true&parentHost=square1.io&parentUrl=square1.io"></amp-playbuzz>',

            // data game after data-embed-by
            '<div class="pb_feed" data-height="auto" data-tags="All" data-recommend="false" data-margin-top="0" data-game-info="true" data-comments="false" data-shares="true" data-embed-by="7d84fda5-2c5f-4c8a-b335-0e11a71497b7" data-game="/pjbrowne10/can-you-identify-the-lesser-known-irish-premier-league-player" data-key="Default"></div>' =>
            '<amp-playbuzz width=300 height=300 layout="responsive" src="https://playbuzz.com/pjbrowne10/can-you-identify-the-lesser-known-irish-premier-league-player?feed=true&src=http://playbuzz.com/pjbrowne10/can-you-identify-the-lesser-known-irish-premier-league-player&game=pjbrowne10/can-you-identify-the-lesser-known-irish-premier-league-player&social=true&parentHost=square1.io&parentUrl=square1.io"></amp-playbuzz>',

            // data item before data-embed-by
            '<div class="pb_feed" data-height="auto" data-item="can-you-identify-the-lesser-known-irish-premier-league-player" data-tags="All" data-recommend="false" data-margin-top="0" data-game-info="true" data-comments="false" data-shares="true" data-embed-by="7d84fda5-2c5f-4c8a-b335-0e11a71497b7" data-key="Default"></div>' =>
            '<amp-playbuzz width=300 height=300 layout="responsive" data-item="can-you-identify-the-lesser-known-irish-premier-league-player"></amp-playbuzz>',

            // data item after data-embed-by
            '<div class="pb_feed" data-height="auto" data-tags="All" data-recommend="false" data-margin-top="0" data-game-info="true" data-comments="false" data-shares="true" data-embed-by="7d84fda5-2c5f-4c8a-b335-0e11a71497b7" data-item="can-you-identify-the-lesser-known-irish-premier-league-player" data-key="Default"></div>' =>
            '<amp-playbuzz width=300 height=300 layout="responsive" data-item="can-you-identify-the-lesser-known-irish-premier-league-player"></amp-playbuzz>',

            // data id after class
            '<div class="playbuzz" data-id="a6bfbd04-125a-4fb8-8ab1-c29eb62e7c2d" data-show-info="false" data-comments="false" data-show-share="false"></div>' =>
            '<amp-playbuzz width=300 height=300 layout="responsive" data-item="a6bfbd04-125a-4fb8-8ab1-c29eb62e7c2d"></amp-playbuzz>',

            // data id before class
            '<div data-id="a6bfbd04-125a-4fb8-8ab1-c29eb62e7c2d" data-show-info="false" data-comments="false" class="playbuzz" data-show-share="false"></div>' =>
            '<amp-playbuzz width=300 height=300 layout="responsive" data-item="a6bfbd04-125a-4fb8-8ab1-c29eb62e7c2d"></amp-playbuzz>'
        ];
    }
}
