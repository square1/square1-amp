<?php

namespace Tests;

use Square1\Amp\AmpPost;
use Square1\Amp\Transformers\PublisherPlusTransformer;

/**
* AMP formatter test
*/
class AvoidUrlWithWordCleaTest extends TestCase
{

    public function testAvoidUrlWithWordClea()
    {
        $post = $this->getPost($this->getUrlWithWordClea());

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'removeInvalidAttributes', [$post['content']['formatted'], 'square1.io']);

        $this->assertEquals($this->getUrlWithWordCleaWithoutRemoving(), $formatted);
    }

    private function getUrlWithWordCleaWithoutRemoving()
    {
        return '<a href="http://www.brownthomas.com/storage+cleaning/wine-rack/invt/64x5620xccbwr&amp;bklist=icat,4,living,living-trends,irish-designers-living">link</a>';
    }

    private function getUrlWithWordClea()
    {
        return '<a href="http://www.brownthomas.com/storage+cleaning/wine-rack/invt/64x5620xccbwr&amp;bklist=icat,4,living,living-trends,irish-designers-living">link</a>';
    }
}