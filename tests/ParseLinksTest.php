<?php

namespace Tests;

use Square1\Amp\AmpPost;
use Square1\Amp\Transformers\PublisherPlusTransformer;

/**
* AMP formatter test
*/
class ParseLinksTest extends TestCase
{

    public function testParseLinkTest()
    {
        $post = $this->getPost($this->getLink());

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'cleanLinks', [$post['content']['formatted'], 'square1.io']);

        $this->assertEquals($this->getLinkFormatted(), $formatted);
    }

    private function getLinkFormatted()
    {
        return '<a href="square1.io"  >';
    }

    private function getLink()
    {
        return '<a href="square1.io" target="_hplink" alt="some alt tag">';
    }
}
