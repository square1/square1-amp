<?php

namespace Tests;

use Square1\Amp\AmpPost;
use Square1\Amp\Transformers\PublisherPlusTransformer;

/**
* AMP formatter test
*/
class ParseMapTagsTest extends TestCase
{

    public function testMapTagsAreNotAllowed()
    {
        $post = $this->getPost($this->getMap());

        $transformer = new PublisherPlusTransformer($post);

        $unsupportedContent = $this->invokeMethod($transformer, 'getUnsupportedContent', ['square1.io']);

        $formatted = $this->invokeMethod($transformer, 'removeMapTags', [$post['content']['formatted'], 'square1.io']);

        $this->assertEquals($unsupportedContent, $formatted);
    }

    private function getMap()
    {
        return '<map width=486 height=657 src="square1.io"></map>';
    }
}
