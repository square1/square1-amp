<?php

namespace Tests;

use Square1\Amp\AmpPost;
use Square1\Amp\Transformers\PublisherPlusTransformer;

/**
* AMP formatter test
*/
class ParseEmptyLinesTest extends TestCase
{

    public function testEmptyLinesShouldBeRemoved()
    {
        $post = $this->getPost($this->getHtml());

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'cleanEmptyLines', [$post['content']['formatted']]);

        $this->assertEquals($this->getHtmlFormatted(), $formatted);
    }

    private function getHtmlFormatted()
    {
        return '<p>some text</p>';
    }

    private function getHtml()
    {
        return '<p>&nbsp;</p><p>some text</p><p>&nbsp;</p>';
    }
}
