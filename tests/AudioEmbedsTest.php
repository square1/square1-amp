<?php

namespace Tests;

use Square1\Amp\AmpPost;
use Square1\Amp\Transformers\PublisherPlusTransformer;

/**
* AMP formatter test
*/
class AudioEmbedsTest extends TestCase
{
    public function testParseAudioEmbeds()
    {
        $post = $this->getPost($this->getAudioEmbed());

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'parseAudioTags', [$post['content']['formatted']]);

        $this->assertEquals($this->getFormattedAudio(), $formatted);

        $this->assertTrue(is_int(strpos($transformer->getScripts(), 'amp-audio')), true);
    }

    private function getAudioEmbed()
    {
        return '<audio src="https://media.balls.ie/uploads/2014/11/GAA-books-podcast-with-Michael-Foley-28-11-2014.mp3" controls="controls"><a href="https://media.balls.ie/uploads/2014/11/GAA-books-podcast-with-Michael-Foley-28-11-2014.mp3">Listen here</a></audio>';
    }

    private function getFormattedAudio()
    {
        return '<amp-audio width="400" height="60" src="https://media.balls.ie/uploads/2014/11/GAA-books-podcast-with-Michael-Foley-28-11-2014.mp3">
                            <div fallback><a href="https://media.balls.ie/uploads/2014/11/GAA-books-podcast-with-Michael-Foley-28-11-2014.mp3">Listen here</a></div>
                        </amp-audio>';
    }
}
