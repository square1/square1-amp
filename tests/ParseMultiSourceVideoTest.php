<?php

namespace Tests;

use Square1\Amp\AmpPost;
use Square1\Amp\Transformers\PublisherPlusTransformer;

/**
* AMP formatter test
*/
class ParseMultiSourceVideoTest extends TestCase
{
    public function testParseMultiSourceVideo()
    {
        $post = $this->getPost($this->getVideo());

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'parseMultiSourceVideo', [$post['content']['formatted']]);

        $this->assertEquals($this->getFormattedVideo(), $formatted);

        $this->assertTrue(is_int(strpos($transformer->getScripts(), 'amp-video')), true);
    }

    private function getVideo()
    {
        return '<video width="620" height="340" controls="controls"><source src="http://a.yfrog.com/img820/1654/eba.mp4" type="video/mp4"/><source src="movie.ogg" type="video/ogg"></source><strong>Some elements are not supported yet for this mobile version. </strong><p><a href="https://www.balls.ie/football/the-exact-moment-lee-dixons-relationship-with-alan-shearer-ended-52619">You can see the full post content here.</a></p></video>';
    }

    private function getFormattedVideo()
    {
        return '<amp-video controls
                            width="640"
                            height="360"
                            layout="responsive"><source src="http://a.yfrog.com/img820/1654/eba.mp4" type="video/mp4"/><source src="movie.ogg" type="video/ogg"/><div fallback>
                                <p>This browser does not support the video element.</p>
                            </div>
                        </amp-video>';
    }
}
