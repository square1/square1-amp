<?php

namespace Tests;

use Square1\Amp\Transformers\PublisherPlusTransformer;

/**
 * AMP formatter test
 */
class ChangeValueTargetAttributeTest extends TestCase
{

    public function testRemoveUnsupportedJavascriptsEvents()
    {
        $post = $this->getPost($this->getContentWithErrorTargetAttribute());

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'changeValueTargetAttribute', [$post['content']['formatted']]);

        $this->assertEquals($this->getContentWithoutErrorTargetAttribute(), $formatted);
    }

    private function getContentWithErrorTargetAttribute()
    {
        return '<a target="blank" href="http://www.balls.ie">mytest.com</a>some text<a target="blank" href="http://www.testing.ie">mytest2.com</a>some text
            <a target="_blank" href="http://www.google.com">Google</a>';
    }

    private function getContentWithoutErrorTargetAttribute()
    {
        return '<a target="_blank" href="http://www.balls.ie">mytest.com</a>some text<a target="_blank" href="http://www.testing.ie">mytest2.com</a>some text
            <a target="_blank" href="http://www.google.com">Google</a>';
    }
}