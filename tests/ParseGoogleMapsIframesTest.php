<?php

namespace Tests;

use Square1\Amp\AmpPost;
use Square1\Amp\Transformers\PublisherPlusTransformer;

/**
* AMP formatter test
*/
class ParseGoogleMapsIframesTest extends TestCase
{

    public function testGoogleMapsIframesShouldUseAmpTag()
    {
        $post = $this->getPost($this->getGoogleMapsIframe());

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'parseGoogleMapsIframes', [$post['content']['formatted']]);

        $this->assertEquals($this->getGoogleMapsIframeFormatted(), $formatted);

        // iframe script should be loaded
        $this->assertTrue(is_int(strpos($transformer->getScripts(), 'amp-iframe')), true);

    }

    private function getGoogleMapsIframe()
    {
        return '<p>Along this section of the route you’ll come across some beautiful mountain scenery, which is also surrounded by vast bog lands. About five minutes into the drive you’ll get to Lough Bray Lower, where you can stop off and get some great photos of the area.</p><p>Twenty minutes later, you’ll reach the ‘spot’ known as view point, which will lead you back towards Knocklyon and onto the M50 at exit 12. You\'re now homeward bound.</p><p><iframe src="https://maps.google.com/maps" width="400" height="350" frameborder="0"></iframe><br><small><a style="color:#0000FF;text-align:left" href="https://maps.google.com/maps">View Larger Map</a></small></p>';
    }

    private function getGoogleMapsIframeFormatted()
    {
        return '<p>Along this section of the route you’ll come across some beautiful mountain scenery, which is also surrounded by vast bog lands. About five minutes into the drive you’ll get to Lough Bray Lower, where you can stop off and get some great photos of the area.</p><p>Twenty minutes later, you’ll reach the ‘spot’ known as view point, which will lead you back towards Knocklyon and onto the M50 at exit 12. You\'re now homeward bound.</p><p><amp-iframe width="500"
                height="281"
                layout="responsive"
                sandbox="allow-scripts allow-popups allow-same-origin"
                allowfullscreen
                frameborder="0"
                src="https://maps.google.com/maps">
            </amp-iframe><br><small><a style="color:#0000FF;text-align:left" href="https://maps.google.com/maps">View Larger Map</a></small></p>';
    }

}