<?php

namespace Tests;

use Square1\Amp\AmpPost;
use Square1\Amp\Transformers\PublisherPlusTransformer;

/**
* AMP formatter test
*/
class ParseIframesTest extends TestCase
{
    public function testIframesShouldUseAmpTag()
    {
        $post = $this->getPost($this->getIframe());

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'parseIframes', [$post['content']['formatted']]);

        $this->assertEquals($this->getIframeFormatted(), $formatted);

        // iframe script should be loaded
        $this->assertTrue(is_int(strpos($transformer->getScripts(), 'amp-iframe')), true);
    }

    private function getIframe()
    {
        return '<iframe width="486" height="657" src="square1.io"></iframe>';
    }

    private function getIframeFormatted()
    {
        return '<amp-iframe width="500"
                    height="657"
                    layout="responsive"
                    sandbox="allow-scripts allow-popups allow-same-origin"
                    allowfullscreen
                    frameborder="0"
                    src="https://square1.io">
                </amp-iframe>';
    }
}
