<?php

namespace Tests;

use Square1\Amp\AmpPost;
use Square1\Amp\Transformers\PublisherPlusTransformer;

/**
* AMP formatter test
*/
class OoyalaEmbedsTest extends TestCase
{

    public function testVimeoShouldUseAmpTag()
    {
        $post = $this->getPost($this->getOoyalaEmbed());

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'parseOoyalaEmbeds', [$post['content']['formatted'], 'www.square1.io']);

        $this->assertEquals($this->getOoyalaFormatted(), $formatted);

        // iframe script should be loaded
        $this->assertTrue(is_int(strpos($transformer->getScripts(), 'amp-iframe')), true);
    }

    private function getOoyalaEmbed()
    {
        return '<script src="http://player.ooyala.com/iframe.js#ec=M3NmZoMzE6hTLD4X_mHQV5dPYA3aowmJ&amp;pbid=86304b0649bd45fab19c6829b518596d"></script>';
    }

    private function getOoyalaFormatted()
    {
        return '<amp-iframe width=300 height=300 '.
                'sandbox="allow-scripts allow-same-origin allow-popups allow-popups-to-escape-sandbox" '.
                'layout="responsive" '.
                'frameborder="0" '.
                'src="https://player.ooyala.com/iframe.html#ec=M3NmZoMzE6hTLD4X_mHQV5dPYA3aowmJ&amp;pbid=86304b0649bd45fab19c6829b518596d&docUrl=www.square1.io">'.
            '</amp-iframe>';
    }
}
