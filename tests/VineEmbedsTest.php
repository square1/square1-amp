<?php

namespace Tests;

use Square1\Amp\AmpPost;
use Square1\Amp\Transformers\PublisherPlusTransformer;

/**
* AMP formatter test
*/
class VineEmbedsTest extends TestCase
{

    public function testVinesShouldUseAmpTag()
    {
        $post = $this->getPost($this->getVine());

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'parseVineEmbeds', [$post['content']['formatted']]);

        $this->assertEquals($this->getVineFormatted(), $formatted);

        // twitter script should be loaded
        $this->assertTrue(is_int(strpos($transformer->getScripts(), 'amp-vine')), true);
    }

    private function getVine()
    {
        return '<iframe class="vine-embed" src="https://vine.co/v/iZIHQgZXVmj/embed/postcard" width="320" height="320" frameborder="0"></iframe><script async src="//platform.vine.co/static/scripts/embed.js" charset="utf-8"></script>';
    }

    private function getVineFormatted()
    {
        return '<amp-vine width="320" height="320" data-vineid="iZIHQgZXVmj" layout="responsive"></amp-vine>';
    }
}
