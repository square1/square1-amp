<?php

namespace Tests;

use Square1\Amp\AmpPost;
use Square1\Amp\Transformers\PublisherPlusTransformer;

/**
* AMP formatter test
*/
class WufooEmbedsTest extends TestCase
{

    public function testWufooIframesShouldUseAmpTag()
    {
        $post = $this->getPost($this->getWufooIframe());

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'parseWufooEmbeds', [$post['content']['formatted'], 'www.square1.io']);

        $this->assertEquals($formatted, $this->getWufooIframeFormatted());

    }

    public function testWufooScriptsAreNotSupported()
    {
        $post = $this->getPost($this->getWufooScript());

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'parseWufooEmbeds', [$post['content']['formatted'], 'www.square1.io']);

        $unsupporteContent = $this->invokeMethod($transformer, 'getUnsupportedContent', ['www.square1.io']);

        $this->assertEquals(trim($formatted), trim($unsupporteContent));

    }

    private function getWufooIframe()
    {
        return '<iframe style="width: 100%; border: none;" src="https://ballsgaffer.wufoo.com/embed/wppz7dw0naf4s1/" width="300" height="380" frameborder="0" scrolling="no"><a href="https://ballsgaffer.wufoo.com/forms/wppz7dw0naf4s1/">Fill out my Wufoo form!</a></iframe>';
    }

    private function getWufooIframeFormatted()
    {
        return '<amp-iframe width="500"
                height="281"
                layout="responsive"
                sandbox="allow-scripts allow-popups allow-same-origin"
                allowfullscreen
                frameborder="0"
                src="https://ballsgaffer.wufoo.com/embed/wppz7dw0naf4s1/">
            </amp-iframe>';
    }

    private function getWufooScript()
    {
        return '<div id="wufoo-m14fohjx1i7zlr3">Who\'ll be the top scorer at Euro 2016, fill out the <a href="https://ballsgaffer.wufoo.com/forms/m14fohjx1i7zlr3">online form here</a>.</div>
<script type="text/javascript">// <![CDATA[
var m14fohjx1i7zlr3;(function(d, t) { var s = d.createElement(t), options = { \'userName\':\'ballsgaffer\', \'formHash\':\'m14fohjx1i7zlr3\', \'autoResize\':true, \'height\':\'260\', \'async\':true, \'host\':\'wufoo.com\', \'header\':\'show\', \'ssl\':true}; s.src = (\'https:\' == d.location.protocol ? \'https://\' : \'http://\') + \'www.wufoo.com/scripts/embed/form.js\'; s.onload = s.onreadystatechange = function() { var rs = this.readyState; if (rs) if (rs != \'complete\') if (rs != \'loaded\') return; try { m14fohjx1i7zlr3 = new WufooForm();m14fohjx1i7zlr3.initialize(options);m14fohjx1i7zlr3.display(); } catch (e) {}}; var scr = d.getElementsByTagName(t)[0], par = scr.parentNode; par.insertBefore(s, scr); })(document, \'script\');
// ]]></script>';
    }
}
